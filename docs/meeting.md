# Summary

## What we want to achieve
* Work we free and/or open source software
* Experience new technical field
* Create a simple game
* testing online capability
  
## Godot Engine
We decided to use godot engine for those reason:
* require less ressource 
* open source
* trending
  
## The Game
* 2D top view
* spaceship
* online pvp / shoot them up
* 3 factions
* earn point
  1. Own permanent
  2. Own lootable
  3. Shared with your faction
* Ships upgrades
  
## Wiki
We will use framagit, but we are open to other suggestions.

## What do we need
In one word: everything. Here some suggestions
* Asset: drawing or [sprite](https://assetforge.io/)
* Elaborate the game description
* Road Map

## Useful links
* [Markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* [Asset](https://assetforge.io/)